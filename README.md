# Introducing weather warning!

![N|Solid](https://f001.backblazeb2.com/file/police-video/Screen+Shot+2018-09-08+at+6.38.47+PM.png)

# The Premise
I took the main idea of the weather app and wrapped it around a skeleton of the Care by Volvo app.
The premise is that a real life Volvo user should be able to get weather updates right from their smartphone to see driving conditions. 

# The Synopsis
The user walks through and selects their current location, at which point they are prompted with tomorrows weather information as well as air pressure and wind speed.


# Framework
I decided to use [Alamofire](https://www.google.com/search?q=alamofire&oq=alamo&aqs=chrome.0.69i59l3j0j69i57j69i60.1507j0j4&sourceid=chrome&ie=UTF-8) to aid in the API calls because it makes it very simple. This is all controlled through the Weather Manager object which allows the developers to query weather and receive all the information back in a dictionary. I wanted a very simple way for other developers to interact with the API. Therefore I added the ability to search simply by name. The Weather Manager class can take one or multiple words.


## Installation
### CocoaPods!

```sh
cd weather_test
pod install
open `Volvo Test.xcworkspace`
```
## Notes on running:
Please use an iPhone 6, 7 , 6 sim. Not tested on plus versions.
# Final Thoughts
If you have any questions please do not hesitate to reach out!

I forced myself to ONLY spend one full day working on this.
If I were to spend more time on this I would completely redo the UI to implement more fluid animations.

Thanks!
