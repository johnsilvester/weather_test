//
//  WeatherManager.swift
//  Volvo Test
//
//  Created by John Silvester on 9/8/18.
//

import UIKit
import Alamofire // Cocoapods library

class WeatherManager: NSObject {
    private let day = Double(60*60*24) // Used in getWeatherByName to push date up one day.
    override init() {
        super.init()
    }

    /**
     Returns all data based on given name.
     - Parameter name: A string of the FULL name. It does not have to be trimmed. This will automagically trim it :)
     - completion: returns all data from metaweather, if it fails, it'l return an empty dic.
     - Note: This is the only public function in the object.
     */
    func getWeatherByName(name: String, completionhandler: @escaping (_ result: [String:Any]) -> Void){
        var trimmedName = name
        if let spaceLocation = trimmedName.range(of: " ") {//Checks and removes spaces in URL, otherwise apple URL wont accept.
            trimmedName.removeSubrange(spaceLocation.lowerBound..<trimmedName.endIndex)
        }
        
        //First call get WoeId to get the Where On Earth Idea. This is used to gather more information about the weather.
        getWoeId(name: trimmedName, completionhandler: { (woeid) in
            
            //Check to see if the city was not found in Metaweather's Database. Will return a blank dictionary and print error message.
            if (woeid < 0){
                print("Error: Cannot locate \(name)")
                completionhandler([:])
            }
            
            //If city is found, grab tomorrows date using built in Date library. NOTE: Self.day is equal to exactly 24 hours.
            let date = Date().addingTimeInterval(self.day)
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy/MM/dd" //Format date in the way Metaweather requires.
            let tomorrowDate = formatter.string(from: date)
            let urlString = "https://www.metaweather.com/api/location/\(woeid)/\(tomorrowDate)/" //Combine woeId and date into url string for MetaWeather to read.
            
            //Call Metaweather API and receive back JSON converted into dictionary.
            self.callApiWithString(urlString: urlString, completionhandler: {(fullWeatherData) in
                completionhandler(fullWeatherData)
                
            })
        })
    }
    /**
     This private function retreives the woe id (Where On Earth Idea)
     - Parameter name: A string of the TRIMMED name. Refer to function above.
     - completion: returns a successful woeid, if anything goes wrong it'll give back a -1
     - Note: WoeID is required for getting any information. That is why this is called first.
     */
    private func getWoeId(name: String, completionhandler: @escaping (_ woeId: Int) -> Void){
        let urlString = "https://www.metaweather.com/api/location/search/?query=\(name)"
        callApiWithString(urlString: urlString, completionhandler: {(returnedData) in
            if (returnedData["woeid"] != nil){
                let woeid = returnedData["woeid"] as! Int
                completionhandler(woeid)
            } else {
                completionhandler(-1)
            }
        })
    }
    
    /**
     This is a direct call to the Metaweather API.
     - Parameter urlString: A string that SHOULD be in the form of a URL with NO SPACES.
     - completion This is return any raw json data from Metaweather.
     - Note: Metaweather wraps their json in an array. Line 72 double unwraps becuase of this.
     */
    private func callApiWithString(urlString: String, completionhandler:  @escaping (_ result: [String:Any]) -> Void){
        Alamofire.request(urlString).responseJSON { response in
            if let json = response.result.value as? [[String: Any]] { //Special json case
                if (json.first != nil){ //Check if response data returned
                    completionhandler(json.first!)
                }else{
                    completionhandler([:])
                }
            }
        }
    }
}
