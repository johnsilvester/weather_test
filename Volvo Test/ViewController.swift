//
//  ViewController.swift
//  Volvo Test
//
//  Created by John Silvester on 9/8/18.
//

import UIKit
import MapKit

class ViewController: UIViewController {
    
    
    //High Level UI Elements
    @IBOutlet var topMenuButton: UIButton!
    @IBOutlet var locationMenuView: UIView!
    @IBOutlet var viewDetailsButton: UIButton!
    @IBOutlet var scrollView: UIScrollView!
    
    //Page 3 UI elements
    @IBOutlet var areYouInLabel: UILabel!
    @IBOutlet var mapView: MKMapView!
    @IBOutlet var changeLocationButton: UIButton!
    
    //Page 2 UI elements
    @IBOutlet var cityNameLabel: UILabel!
    @IBOutlet var weatherStateLabel: UILabel!
    @IBOutlet var iconImageView: UIImageView!
    @IBOutlet var temperatureLabel: UILabel!
    @IBOutlet var messageLabel: UILabel!
    
    //Page 3 UI elements
    @IBOutlet var selectLocationLabel: UIView!
    @IBOutlet var leftDetailInfoLabel: UILabel!
    @IBOutlet var rightDetailInfoLabel: UILabel!
    @IBOutlet var leftDetailLabel: UILabel!
    @IBOutlet var rightDetailLabel: UILabel!
    @IBOutlet var leftDetailImageView: UIImageView!
    @IBOutlet var rightDetailImageView: UIImageView!
    @IBOutlet var homeButton: UIButton!
    
    
    //A dictionary for getting Lat & Long of required locations. Used for MapKit
    private var locations = ["New York": ["Long":-74.0025,"Lat":40.721],
                             "Stockholm": ["Long":18.068580800000063,"Lat":59.32932349999999],
                             "Mountain View": ["Long":-122.083851,"Lat":37.3860517],
                             "London": ["Long":-0.12775829999998223,"Lat":51.5073509],
                             "Berlin": ["Long":13.404953999999975,"Lat":52.52000659999999],
                             "Gothenburg": ["Long":11.974559999999997,"Lat":57.70887]
    ]
    //A dictionary that has the icon image name and a simple message to be displayed to the user at the weather display screen
    private var weatherInfo = ["Snow":["Icon":"sn","Message":"Roads will be slippery"],
                               "Sleet":["Icon":"sl","Message":"Roads will be slippery"],
                               "Hail":["Icon":"h","Message":"Be safe, your car could get damaged"],
                               "Thunderstorm":["Icon":"t","Message":"Bring an umbrella"],
                               "Heavy Rain":["Icon":"hr","Message":"Bring an umbrella"],
                               "Light Rain":["Icon":"lr","Message":"Bring a coat"],
                               "Showers":["Icon":"s","Message":"A small sprinkle"],
                               "Heavy Cloud":["Icon":"hc","Message":"It'll be gloomy out"],
                               "Light Cloud":["Icon":"lc","Message":"Have a great day!"],
                               "Clear":["Icon":"c","Message":"Have a great day!"]
    ]
    
    
    let manager = WeatherManager()//Manages metaweather api calls.
    var pageCounter = 1 //Keeps track of 'pages' on scrollview
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // load mountainview as default location for map.
        let coordinates = locations["Mountain View"]
        mapView.setCenter(CLLocationCoordinate2DMake(Double(coordinates!["Lat"]!), Double(coordinates!["Long"]!)), animated: true)
    }
    
    //MARK: Actions
    /**
     Fake Volvo menu is a button. The tag switches between the two images,
     each represent a fake menu. The first is "issues" the second is "Statistics"
     Top Menu = Issues highlighted
     Top Menu alt = Statistics highlighted
     - Notes: sender.tag never gets reset.
     */
    @IBAction func topMenuClicked(_ sender: UIButton) {
        if (sender.tag % 2 == 0){
            sender.setImage(UIImage.init(named: "Top Menu"), for: UIControlState.normal)
        }else{
            sender.setImage(UIImage.init(named: "Top Menu alt"), for: UIControlState.normal)
        }
        sender.tag += 1
    }
    
    /**
     This button is located on the page when you open the app, under the XC40.
     It simply advances to the next page
     */
    @IBAction func viewDetailsButtonClicked(_ sender: UIButton) {
        scrollToNextPage()
    }
    
    /**
     This button brings up the menu of all locations, called "location menu view".
     It alls brings the menu to the front to make sure it's the most visible element.
     */
    @IBAction func changeLocationButtonPressed(_ sender: UIButton) {
        locationMenuView.isHidden = false
        self.view.bringSubview(toFront: locationMenuView)
    }
    
    /**
     MULTI-CONNECTED BUTTON. This button is connected to every button on the location menu view.
     It uses a switch statement to omit a ton of redundent code.
     This method is essentially the control switch.
     */
    @IBAction func locationButtonPressed(_ sender: UIButton) {
        switch sender.tag {
        case 0: //Gothenburg
            retreiveAndSetWeatherData(for: "Gothenburg")
        case 1: //Stockholm
            retreiveAndSetWeatherData(for: "Stockholm")
        case 2: //Mountain View
            retreiveAndSetWeatherData(for: "Mountain View")
        case 3: //London
            retreiveAndSetWeatherData(for: "London")
        case 4: //New York
            retreiveAndSetWeatherData(for: "New York")
        case 5: //Berlin
            retreiveAndSetWeatherData(for: "Berlin")
        default:
            retreiveAndSetWeatherData(for: "Gothenburg")
        }
    }
    
    /**
     Essentially the reset button. Returns the user to the beginning of the app experience.
     - Note: Page counter gets reset
     */
    @IBAction func homeButtonClicked(_ sender: Any) {
        scrollView.setContentOffset(CGPoint(x: 0, y:0),animated: false)
        pageCounter = 1
    }
    
    //MARK: Helper Methods
    /**
     Scrolls to new 'page' on scrollview.
     UI scrollview has 3 pages which control the flow of the app.
     Finds a new page by scroll the height of the total scrollview.
     - Notes: pageCounter keeps track of current page.
     */
    func scrollToNextPage(){
        scrollView.setContentOffset(CGPoint(x: 0, y: scrollView.frame.height * CGFloat(pageCounter) ),animated: true)
        pageCounter += 1
    }
    
    /**
     Retreives weather data from API based on location given.
     Sets all values on the third page.
     This is a completion
     Sets:
     temp
     air pressure
     wind speeed
     location
     message
     - Parameter location: The location in the form of a string.
     */
    func retreiveAndSetWeatherData(for location: String){
        manager.getWeatherByName(name: location, completionhandler:{(allData) in
            //Safety check if there is an error with API
            if (allData.keys.count == 0){
                print("There was an error retreiving data.")
                self.messageLabel.text = "Something Went wrong!" // Will scroll to next page and display something went wrong.
                self.weatherStateLabel.text = "API ERROR"
                self.scrollToNextPage()
            }else{
                //retreive data from API and set to vars.
                let windSpeed = allData["wind_speed"] as! Double
                let airPressure = allData["air_pressure"] as! Double
                let temp = allData["the_temp"] as! Double
                let weather = allData["weather_state_name"] as! String
                let iconName = self.weatherInfo[weather]!["Icon"]
                let message = self.weatherInfo[weather]!["Message"]
                
                //set labels and other UI elements based on data. Finally, scroll the page.
                self.leftDetailInfoLabel.text = "\(Double(floor(1000*windSpeed)/1000))"
                self.rightDetailInfoLabel.text = "\(Double(floor(1*airPressure)/1))"
                self.temperatureLabel.text = "\(Double(floor(1000*temp)/1000)) C"
                self.weatherStateLabel.text = weather.uppercased()
                self.iconImageView.image = UIImage.init(named: iconName!)
                self.messageLabel.text = message!
                self.scrollToNextPage()
            }})
        
        //non-essential data setting. This changes the map to show the location the user selected.
        //NOTE: this is get called before the code above does becuase it's a completion handler!
        let coordinates = locations[location]
        mapView.setCenter(CLLocationCoordinate2DMake(Double(coordinates!["Lat"]!), Double(coordinates!["Long"]!)), animated: true)
        locationMenuView.isHidden = true
        cityNameLabel.text = location.uppercased()
        areYouInLabel.text = "ARE YOU IN \(location.uppercased())?"
        
    }
    
    
}

